var gulp = require('gulp');
var plugins = require("gulp-load-plugins")({
    pattern: ['gulp-*', 'gulp.*'],
    replaceString: /\bgulp[\-.]/
});

var plumberErrorHandler = {
    errorHandler: plugins.notify.onError({
        title: 'Gulp',
        message: 'Error: <%= error.message %>'
    })
};

var paths = {
    src: './src/',
    dist: './dist/',
    bowerDir: './bower_components/'
};

gulp.task('bower', function(){
    return plugins.bower()
        .pipe(gulp.dest(paths.bowerDir));
});

gulp.task('clean', function(){
    return gulp.src(paths.dist)
        .pipe(plugins.rimraf())
});

gulp.task('css', function(){
    gulp.src(paths.src + 'styles/*.less')
        .pipe(plugins.plumber(plumberErrorHandler))
        .pipe(plugins.sourcemaps.init())
        .pipe(plugins.less())
        .pipe(plugins.sourcemaps.write())
        .pipe(plugins.size({title: 'css'}))
        .pipe(gulp.dest(paths.dist + 'styles/'));
});

gulp.task('js', function(){
    gulp.src(paths.src + 'js/*.js')
        .pipe(plugins.plumber(plumberErrorHandler))
        .pipe(plugins.jshint())
        .pipe(plugins.jshint.reporter('fail'))
        .pipe(plugins.sourcemaps.init())
        .pipe(plugins.uglify())
        .pipe(plugins.concat('main.js'))
        .pipe(plugins.sourcemaps.write())
        .pipe(plugins.size({title: 'js'}))
        .pipe(gulp.dest(paths.dist + 'js/'));
});

gulp.task('img', function(){
    gulp.src(paths.src + 'img/*.{png,jpg,gif}')
        .pipe(plugins.plumber(plumberErrorHandler))
        .pipe(plugins.imagemin({
            optimizationLevel: 7,
            progressive: true
        }))
        .pipe(plugins.size({title: 'img'}))
        .pipe(gulp.dest(paths.dist + 'img/'));
});

gulp.task('html', function(){
    gulp.src(paths.src + '*.html')
        .pipe(plugins.plumber(plumberErrorHandler))
        .pipe(plugins.size({title: 'html'}))
        .pipe(gulp.dest(paths.dist));
});

gulp.task('fonts', function(){
    gulp.src(paths.src + 'fonts/**.*')
        .pipe(plugins.plumber(plumberErrorHandler))
        .pipe(plugins.size({title: 'fonts'}))
        .pipe(gulp.dest(paths.dist + 'fonts/'));
});

gulp.task('watch', function(){
    gulp.watch(paths.src + '*.html', ['html']);
    gulp.watch(paths.src + 'styles/*.less', ['css']);
    gulp.watch(paths.src + 'js/*.js', ['js']);
    gulp.watch(paths.src + 'img/*.{png,jpg,gif}', ['img']);
});

gulp.task('serve', ['default'], function() {
    plugins.connect.server({
        root: 'dist'
    });
});

gulp.task('default', ['html', 'fonts', 'css', 'js', 'img', 'watch']);
gulp.task('build', ['html', 'fonts', 'css', 'js', 'img']);


